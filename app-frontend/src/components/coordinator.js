import React, { Component } from 'react';
import {FormattedMessage} from "react-intl";


class Coordinator extends Component {
    render() {
        return(
            <div> <h2 style={{paddingTop: '2em'}}>Sebestyen-Pal Gheorghe</h2>
                
                <h4 style={{color: 'grey'}}><FormattedMessage id="coordinator.professor"
                                                              defaultMessage="Professor"
                                                              description="Link on react page"/></h4>
                <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
                <p><FormattedMessage id="coordinator.faculty"
                                     defaultMessage="Technical University of Cluj-Napoca, Computers Dep."
                                     description="Link on react page"/></p>
                <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
                <h5><FormattedMessage id="coordinator.homeA"
                                      defaultMessage="Home Address"
                                      description="Link on react page"/></h5>
                <p><FormattedMessage id="coordinator.home"
                                     defaultMessage="str. N. Titulescu nr. 12/6, City: Cluj-Napoca, Romania, Code: RO-3400"
                                     description="Link on react page"/></p>
                <h5>
                    <FormattedMessage id="coordinator.workA"
                                      defaultMessage="Work Address"
                                      description="Link on react page"/></h5>
                <p><FormattedMessage id="coordinator.work"
                                     defaultMessage="Technical University of Cluj, Computers Dep., str. G. Baritiu nr. 26-28, City:  Cluj-Napoca, Romania, Code: RO-3400, Tel: +40 264 401489, Fax: +40-264-594491"
                                     description="Link on react page"/></p>
                <h5><FormattedMessage id="about.phone"
                                      defaultMessage="Phone"
                                      description="Link on react page"/></h5>
                <p>+40-745-362211</p>
                <h5>Email</h5>
                <p>gheorghe.sebestyen@cs.utcluj.ro</p>
                {/*<h5>Web</h5>*/}
                {/*<p>mywebsite.com</p>*/}
                <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/></div>
        )
    }
}

export default Coordinator;
