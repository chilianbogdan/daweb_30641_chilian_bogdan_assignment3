import React, { Component } from 'react';
import { Tabs, Tab, Grid, Cell, Card, CardTitle, CardText, CardActions, Button, CardMenu, IconButton } from 'react-mdl';
import { FormattedMessage } from 'react-intl';
import List from './List'
class Project extends Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: 0,
        term: '',
        items: []
    };

  }

  toggleCategories() {

    if(this.state.activeTab === 0){
      return(
          <div><h1><p>
            <FormattedMessage
                id="project.t"
                defaultMessage="It's a beautiful day outside."
                description="Link on react page"
            />
          </p></h1></div>
        // <div className="projects-grid">
        //   {/* Project 1 */}
        //   <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
        //     <CardTitle style={{color: '#fff', height: '176px', background: 'url(https://xtnotes-1255646395.coshk.myqcloud.com/images/react-1.svg) center / cover'}} >React Project #1</CardTitle>
        //     <CardText>
        //       Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
        //     </CardText>
        //     <CardActions border>
        //       <Button colored>GitHub</Button>
        //       <Button colored>CodePen</Button>
        //       <Button colored>Live Demo</Button>
        //     </CardActions>
        //     <CardMenu style={{color: '#fff'}}>
        //       <IconButton name="share" />
        //     </CardMenu>
        //   </Card>
        //
        //   {/* Project 2 */}
        //   <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
        //     <CardTitle style={{color: '#fff', height: '176px', background: 'url(https://xtnotes-1255646395.coshk.myqcloud.com/images/react-1.svg) center / cover'}} >React Project #2</CardTitle>
        //     <CardText>
        //       Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
        //     </CardText>
        //     <CardActions border>
        //       <Button colored>GitHub</Button>
        //       <Button colored>CodePen</Button>
        //       <Button colored>Live Demo</Button>
        //     </CardActions>
        //     <CardMenu style={{color: '#fff'}}>
        //       <IconButton name="share" />
        //     </CardMenu>
        //   </Card>
        //
        //   {/* Project 3 */}
        //   <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
        //     <CardTitle style={{color: '#fff', height: '176px', background: 'url(https://xtnotes-1255646395.coshk.myqcloud.com/images/react-1.svg) center / cover'}} >React Project #3</CardTitle>
        //     <CardText>
        //       Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
        //     </CardText>
        //     <CardActions border>
        //       <Button colored>GitHub</Button>
        //       <Button colored>CodePen</Button>
        //       <Button colored>Live Demo</Button>
        //     </CardActions>
        //     <CardMenu style={{color: '#fff'}}>
        //       <IconButton name="share" />
        //     </CardMenu>
        //   </Card>
        // </div>


      )
    } else if(this.state.activeTab === 1) {
      return (
        <div><h1><p>
          <FormattedMessage
              id="project.t"
              defaultMessage="It's a beautiful day outside."
              description="Link on react page"
          />
        </p></h1></div>
      )
    } else if(this.state.activeTab === 2) {
      return (
        <div><h1><p>
          <FormattedMessage
              id="project.t"
              defaultMessage="It's a beautiful day outside."
              description="Link on react page"
          />
        </p></h1></div>
      )
    } else if(this.state.activeTab === 3) {
      return (
        <div><h1><p>
          <FormattedMessage
              id="project.t"
              defaultMessage="It's a beautiful day outside."
              description="Link on react page"
          />
        </p></h1></div>
      )
    }

  }
    // displayComments() {
    //     var comments = [];
    //     if (this.state.comments !== null) {
    //         for (let i = 0; i < this.state.comments.length; i++) {
    //             var owned = false;
    //             if (this.state.comments[i].username === AuthenticationService.getLoggedInUserName()) {
    //                 owned = true;
    //             }
    //             comments.push(<div key={this.state.comments[i].id} className="CommentDiv">
    //                 <div className="Comment">
    //                     <span className="CommentUserName">{this.state.comments[i].username} </span>
    //                     <span>{this.state.comments[i].comment}</span>
    //                 </div>
    //                 {owned && <button className="DeleteComment" onClick={() => this.deleteComment(this.state.comments[i].id)}>Delete</button>}
    //             </div>)
    //         }
    //     }
    //     return comments;
    // }
    // handleSubmit(event) {
    //     event.preventDefault();
    //     let comm = {
    //         comment: this.state.userComment
    //     }
    //
    //     var url = window.location.pathname;
    //     const landmark_id = url.substring(url.lastIndexOf('/') + 1);
    //     return API_COMMENTS.insertComment(comm, this.state.userId, landmark_id, (result, status, error) => {
    //
    //         if(status === 200 || status ===201){
    //             window.location.reload();
    //         }
    //     });
    // }




    render() {
    return(
      <div>
        <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
          <Tab><FormattedMessage id="project.summary"
                                 defaultMessage="Summary"
                                 description="Link on react page"/></Tab>
          <Tab><FormattedMessage id="project.content"
                                 defaultMessage="Content"
                                 description="Link on react page"/></Tab>
          <Tab><FormattedMessage id="project.writing"
                                 defaultMessage="Writing"
                                 description="Link on react page"/></Tab>
          <Tab><FormattedMessage id="project.bibliography"
                                 defaultMessage="Bibliography"
                                 description="Link on react page"/></Tab>
        </Tabs>


          <Grid>
            <Cell col={12}>
              <div className="content">{this.toggleCategories()}</div>
            </Cell>
          </Grid>


                      <List />


      </div>
    )
  }
}

export default Project;
