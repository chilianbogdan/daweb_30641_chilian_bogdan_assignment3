import axios from 'axios'

export const getList = () => {
    return axios
        .get('/api/comments', {
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => {
            return res.data
        })
}

export const addItem = comment => {
    return axios
        .post(
            '/api/comments',
            {
                comment: comment
            },
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        .then(function(response) {
            console.log(response)
        })
}

export const deleteItem = id => {
    axios
        .delete(`/api/comments/${id}`, {
            headers: { 'Content-Type': 'application/json' }
        })
        .then(function(response) {
            console.log(response)
        })
        .catch(function(error) {
            console.log(error)
        })
}

export const updateItem = (comment, id) => {
    return axios
        .put(
            `/api/comments/${id}`,
            {
                comment: comment
            },
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        .then(function(response) {
            console.log(response)
        })
}
export const updateUser = (name,password,domeniu, id) => {
    return axios
        .put(
            `/api/editUser/${id}`,
            {
                name: name,
                password: password,
                domeniu: domeniu
            },
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        .then(function(response) {
            console.log(response)
        })
}