import React, { Component } from 'react';
import { Grid, Cell, List, ListItem, ListItemContent } from 'react-mdl';
import {FormattedMessage} from "react-intl";


class Contact extends Component {
  render() {
    return(
      <div className="contact-body">
        <Grid className="contact-grid">
          <Cell col={6}>
            <h2>Bogdan Chilian</h2>
            <img
              src="https://www.facebook.com/chilian.bogdan?viewas=100000686899395&privacy_source=timeline_gear_menu&entry_point=action_bar#_"
              alt="avatar"
              style={{height: '250px'}}
               />
             <p style={{ width: '75%', margin: 'auto', paddingTop: '1em'}}>23 years old</p>
            <div className="emailForm">
              <form action="http://localhost:5000/result" method="get">
                <FormattedMessage id="contact.email"
                                  defaultMessage="Send an email to:"
                                  description="Link on react page"/>
                {" "}
                <input className="textfieldEmail" type="text" name="place" />
                <input className="submitEmail" type="submit" value="Submit" />
              </form>
            </div>
          </Cell>
          <Cell col={6}>
            <h2><FormattedMessage id="contact.contactme"
                                  defaultMessage="Contact Me:"
                                  description="Link on react page"/></h2>
            <hr/>

            <div className="contact-list">
              <List>
                <ListItem>
                  <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                    <i className="fa fa-phone-square" aria-hidden="true"/>
                    0752 684 224
                  </ListItemContent>
                </ListItem>

                {/*<ListItem>*/}
                {/*  <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>*/}
                {/*    <i className="fa fa-fax" aria-hidden="true"/>*/}
                {/*    (123) 456-7890*/}
                {/*  </ListItemContent>*/}
                {/*</ListItem>*/}

                <ListItem>
                  <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                    <i className="fa fa-envelope" aria-hidden="true"/>
                    chilian.bogdan@gmail.com
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                    <i className="fa fa-facebook" aria-hidden="true"/>
                    https://www.facebook.com/chilian.bogdan
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent style={{fontSize: '15px', fontFamily: 'Anton'}}>
                    <i className="fa fa-linkedin" aria-hidden="true"/>
                    https://ro.linkedin.com/in/bogdan-chilian-64b52916b
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                    <i className="fa fa-instagram" aria-hidden="true"/>
                    https://www.instagram.com/chilianbogdan
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                    <i className="fa fa-twitter" aria-hidden="true"/>
                    https://twitter.com/ChilianBogdan
                  </ListItemContent>
                </ListItem>

              </List>
            </div>
          </Cell>
        </Grid>

      </div>
    )
  }
}

export default Contact;
