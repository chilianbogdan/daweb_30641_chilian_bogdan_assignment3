// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import registerServiceWorker from './registerServiceWorker';
// import 'react-mdl/extra/material.css';
// import 'react-mdl/extra/material.js';
// import { BrowserRouter } from 'react-router-dom';
// //import { IntlProvider, addLocaleData } from "react-intl";
// import en from "react-intl/locale-data/en";
// import es from "react-intl/locale-data/es";
//
// import localeData from "./../public/locales/data.json";
//
// //addLocaleData([...en, ...es]);
//
// // Define user's language. Different browsers have the user locale defined
// // on different fields on the `navigator` object, so we make sure to account
// // for these different by checking all of them
// // const language =
// //     (navigator.languages && navigator.languages[0]) ||
// //     navigator.language ||
// //     navigator.userLanguage;
// //
// // // Split locales with a region code
// // const languageWithoutRegionCode = language.toLowerCase().split(/[_-]+/)[0];
// //
// // // Try full locale, try locale without region code, fallback to 'en'
// // const messages =
// //     localeData[languageWithoutRegionCode] ||
// //     localeData[language] ||
// //     localeData.en;
//
// // ReactDOM.render(
// //     //<IntlProvider locale={language} messages={messages}>
// //       <BrowserRouter>
// //         <App />
// //       </BrowserRouter>,
// //    // </IntlProvider>,
// //     document.getElementById("root")
// // );
// // registerServiceWorker();
// ReactDOM.render(
//     <BrowserRouter>
//         <App />
//     </BrowserRouter>
//     , document.getElementById('root'));
// registerServiceWorker();
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';
import { BrowserRouter } from 'react-router-dom';
import {IntlProvider} from "react-intl";
//import { addLocaleData } from "react-intl";
//import locale_en from 'react-intl/locale-data/en';
//import locale_de from 'react-intl/locale-data/de';
import messages_ro from "./ro.json";
import messages_en from "./en.json";
//import { variableOne } from "./App.js";
//addLocaleData([...locale_en, ...locale_de]);


const messages = {
    'ro': messages_ro,
    'en': messages_en
};
//const language = navigator.language.split(/[-_]/)[0];  // language without region code
//var vOneLS = localStorage.getItem("vOneLocalStorage ");

const locale = window.localStorage.getItem('language');;

ReactDOM.render(

    <IntlProvider locale={locale} messages={messages[locale]}>
        <BrowserRouter>
        <App/>
    </BrowserRouter>
    </IntlProvider>,

    document.getElementById('root'));
registerServiceWorker();
